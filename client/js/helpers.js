"use strict";

Template.todos.helpers({
    todoList: function () {
        return Collections.Todo.find({});
    }
});

$(".complete-task").click(function () {
    $("div").animate({left: '250px'});
});